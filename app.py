from flask import Flask, jsonify
import datetime
import decimal
import pymssql
import os
from dotenv import load_dotenv

load_dotenv()
app = Flask(__name__)

# Configuración de la cadena de conexión a la base de datos SQL Azure
conn = pymssql.connect(
    server=os.getenv('DB_SERVER') ,
    user=os.getenv('DB_USER') ,
    password=os.getenv('DB_PASSWORD') ,
    database=os.getenv('DB_DATABASE') ,
    port=int(os.getenv('DB_PORT'))
)

def convert_dict_to_serializable(input_dict):
    output_dict = {}
    for key, value in input_dict.items():
        if isinstance(value, decimal.Decimal):
            output_dict[key] = float(value)
        elif isinstance(value, datetime.datetime):
            output_dict[key] = value.strftime('%Y-%m-%d %H:%M:%S')
        else:
            output_dict[key] = value
    return output_dict

# Ruta para consultar todos los productos
@app.route('/productos')
def obtener_productos():
    cursor = conn.cursor(as_dict=True)
    cursor.execute('SELECT * FROM productos')
    productos = cursor.fetchall()
    cursor.close()
    return jsonify([convert_dict_to_serializable(producto) for producto in productos])

# Ruta para consultar un producto por ID
@app.route('/producto/<int:id>')
def obtener_producto(id):
    cursor = conn.cursor(as_dict=True)
    cursor.execute('SELECT * FROM productos WHERE id=%s', (id,))
    producto = cursor.fetchone()
    cursor.close()
    if producto:
        producto["angel"]="HOLA MUNDO"
        data={"NOMBRE":"ANGEL","APELLIDO":"CAYAOTOPA"}
        return jsonify(convert_dict_to_serializable(data))
    else:
        return jsonify({'mensaje': 'Producto no encontrado'}), 404

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)

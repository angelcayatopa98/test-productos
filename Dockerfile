# Usamos una imagen base de Python
FROM python:3.8

# Establecemos el directorio de trabajo
WORKDIR /app

# Copiamos los archivos de la aplicación al contenedor
COPY . .

# Instalamos las dependencias
RUN pip install -r requirements.txt

EXPOSE 5000

# Comando para ejecutar la aplicación
CMD ["python", "app.py"]
